local t = require("luatest")
local g = t.group("integration_api")

local helper = require("test.helper.integration")
local deepcopy = helper.shared.deepcopy
local r = require("app.utils.response")

local test_obj = {
    key = "1",
    value = {
        info = "Some kind of info",
        status = 1,
    },
}

local expected_value = {
    created = {
        json = { info = r.created_field.msg },
        status = r.created_field.status,
    },
    success = {
        json = { info = r.success_field.msg },
        status = r.success_field.status,
    },
    not_found = {
        json = { info = r.not_found_err_field.msg },
        status = r.not_found_err_field.status,
    },
    duplicate = {
        json = { info = r.key_exists_err_field.msg },
        status = r.key_exists_err_field.status,
    },
    corr_body = {
        json = { info = r.incorrect_body_err_field.msg },
        status = r.incorrect_body_err_field.status,
    },
}

g.test_get_not_found_err = function()
    helper.assert_http_json_request(
        "GET",
        "/kv/1",
        nil,
        expected_value.not_found
    )
end

g.test_post_body_err = function()
    local corrupted = deepcopy(test_obj)
    corrupted.value = nil

    helper.assert_http_json_request(
        "POST",
        "/kv",
        corrupted,
        expected_value.corr_body
    )
end

g.test_post_ok = function()
    helper.assert_http_json_request(
        "POST",
        "/kv",
        test_obj,
        expected_value.created
    )
end

g.test_post_duplicate_err = function()
    helper.assert_http_json_request(
        "POST",
        "/kv",
        test_obj,
        expected_value.duplicate
    )
end

g.test_get_ok = function()
    helper.assert_http_json_request(
        "GET",
        "/kv/1",
        nil,
        { json = test_obj, status = expected_value.success.status }
    )
end

g.test_put_body_err = function()
    helper.assert_http_json_request(
        "PUT",
        "/kv/1",
        { not_value = 5 },
        expected_value.corr_body
    )
end

g.test_put_not_found_err = function()
    helper.assert_http_json_request(
        "PUT",
        "/kv/2",
        { value = { info = "new_value" } },
        expected_value.not_found
    )
end

g.test_put_ok = function()
    local new_obj = deepcopy(test_obj)
    new_obj.value = { info = "new_value" }

    helper.assert_http_json_request(
        "PUT",
        "/kv/1",
        { value = new_obj.value },
        { json = new_obj, status = expected_value.success.status }
    )
end

g.test_delete_not_found_err = function()
    helper.assert_http_json_request(
        "DELETE",
        "/kv/3",
        nil,
        expected_value.not_found
    )
end

g.test_delete_ok = function()
    helper.assert_http_json_request(
        "DELETE",
        "/kv/1",
        nil,
        expected_value.success
    )
end
