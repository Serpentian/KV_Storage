local box = require("box")
local t = require("luatest")
local g = t.group("unit_sample")

local helper = require("test.helper.unit")
local storage = require("app.roles.storage")
local deepcopy = helper.shared.deepcopy

local test_obj = {
    key = "1",
    bucket_id = 1,
    value = {
        info = "Some kind of info",
        status = 1,
    },
}

g.before_all(function()
    storage.init({ is_master = true })
end)

g.before_each(function()
    box.space.kv_storage:truncate()
end)

g.test_object_add_ok = function()
    local to_insert = deepcopy(test_obj)
    t.assert_equals(storage.utils.object_add(to_insert), { ok = true })

    local from_space = box.space.kv_storage:get(test_obj.key)
    t.assert_equals(from_space, box.space.kv_storage:frommap(to_insert))
end

g.test_object_add_err = function()
    box.space.kv_storage:insert(box.space.kv_storage:frommap(test_obj))
    local res = storage.utils.object_add(test_obj)
    if res.error then
        res.error = res.error.err
    end

    t.assert_equals(res, {
        ok = false,
        error = "Key already exists",
    })
end

g.test_object_update_ok = function()
    box.space.kv_storage:insert(box.space.kv_storage:frommap(test_obj))
    local new_obj = {
        key = test_obj.key,
        value = { info = "New info" },
    }

    local res = storage.utils.object_update(new_obj.key, new_obj.value)
    t.assert_equals(res, { obj = new_obj, error = nil })

    new_obj.bucket_id = test_obj.bucket_id
    t.assert_equals(
        box.space.kv_storage:get(test_obj.key),
        box.space.kv_storage:frommap(new_obj)
    )
end

g.test_object_update_err = function()
    local res = storage.utils.object_update(test_obj.key, test_obj.value)
    if res.error then
        res.error = res.error.err
    end

    t.assert_equals(res, {
        obj = nil,
        error = "Object not found",
    })
end

g.test_object_get_ok = function()
    local to_get = deepcopy(test_obj)
    box.space.kv_storage:insert(box.space.kv_storage:frommap(to_get))

    to_get.bucket_id = nil
    t.assert_equals(storage.utils.object_get(test_obj.key), { obj = to_get })
end

g.test_object_get_err = function()
    local res = storage.utils.object_get(test_obj.key)
    if res.error then
        res.error = res.error.err
    end

    t.assert_equals(res, { obj = nil, error = "Object not found" })
end

g.test_object_delete_ok = function()
    box.space.kv_storage:insert(box.space.kv_storage:frommap(test_obj))
    t.assert_equals(storage.utils.object_delete(test_obj.key), { ok = true })
    t.assert_equals(box.space.kv_storage:get(test_obj.key), nil)
end

g.test_object_delete_err = function()
    local res = storage.utils.object_delete(test_obj.key)
    if res.error then
        res.error = res.error.err
    end

    t.assert_equals(res, { ok = false, error = "Object not found" })
end
