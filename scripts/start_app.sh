#! /bin/bash

# The script is supposed to be ran
# inside the docker container

function start_cartridge {
    echo "Launching cartridge..."
    cartridge start -d

    echo "Bootstrapping cartridge..."
    cartridge replicasets setup --bootstrap-vshard;

    echo "failover configuring..."
    cartridge failover setup
}

function start_nginx {
    echo "Configuring nginx..."
    nginx_conf=$(pwd)/nginx.conf

    # $PORT env var is set by Heroku
    sed -i -e 's/$PORT/'"$PORT"'/g' $nginx_conf

    echo "Launching nginx in daemon mode"
    nginx -c $nginx_conf -g 'daemon off;'
}

start_cartridge
start_nginx
