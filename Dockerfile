# Grap CentOS image
FROM centos:7

# Update packages, install dependencies for building
RUN yum -y update
RUN yum install -y epel-release vim unzip
RUN yum install -y git gcc cmake3 cmake make gcc-c++ zlib-devel readline-devel \
        ncurses-devel openssl-devel libunwind-devel libicu-devel

# Installing tarantool and catridge framework
RUN curl -L https://tarantool.io/release/2.8/installer.sh | bash
RUN yum install -y tarantool tarantool-devel cartridge-cli nginx

# Add my code
COPY ./ /home/tarantool
WORKDIR /home/tarantool

# Build cartridge dependencies
RUN cd /home/tarantool && cartridge build --verbose
