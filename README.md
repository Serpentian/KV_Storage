# Key-value storage

This is the simplest [Tarantool](https://www.tarantool.io/en/) Cartridge based application which implements the logic of key-value storage. The project is deployed with [Heroku](https://www.heroku.com/) at the following URL: https://tarantool-kv-storage.herokuapp.com/kv

## Prerequisites

To get key-value storage application and run it, you need to install several packages:
* git, gcc, unzip, make and cmake
* tarantool and tarantool-dev
* cartridge-cli

## Building and launching

To build application and setup topology:

```bash
cartridge build
cartridge start -d
cartridge replicasets setup --bootstrap-vshard
```

Now you can visit http://localhost:8081 and see your application's Admin Web UI.

## API

| Method    | Path      | Request                       |
|-----------|-----------|-------------------------------|
| GET       | /kv/{id}  | nil                           |
| PUT       | /kv/{id}  | {'value': JSON}               |
| POST      | /kv       | {'key': 'str', 'value': JSON} |
| DELETE    | /kv/{id}  | nil                           |

You can try it out with the following commands:

```bash
curl -X POST -v -H "Content-Type: application/json" -d '{"key": "key-1", "value": { "info": "abc" }}' https://tarantool-kv-storage.herokuapp.com/kv
```

```bash
curl -X GET -v -H "Content-Type: application/json" https://tarantool-kv-storage.herokuapp.com/kv/key-1
```

```bash
curl -X PUT -v -H "Content-Type: application/json" -d '{"value": { "data": "cab" }}' https://tarantool-kv-storage.herokuapp.com/kv/key-1
```

```bash
curl -X DELETE -v -H "Content-Type: application/json" https://tarantool-kv-storage.herokuapp.com/kv/key-1
```

## Tests

Unit and integration tests are located in the [`test`](./test) directory.
First, we need to install test dependencies:

```bash
bash ./deps.sh
```

Then, run linter:

```bash
.rocks/bin/luacheck .
```

Now we can run tests:

```bash
cartridge stop  # to prevent "address already in use" error
.rocks/bin/luatest -v
```

## TODO

- [x] Main logic
- [x] Nginx + Cloud Platform
- [ ] CI/CD configuration
- [ ] Static files
