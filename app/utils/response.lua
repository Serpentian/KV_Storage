local Response = {
    internal_err_field = {
        msg = "Internal error",
        status = 500,
    },

    incorrect_body_err_field = {
        msg = "Incorrect request body",
        status = 400,
    },

    key_exists_err_field = {
        msg = "Key already exists",
        status = 409,
    },

    not_found_err_field = {
        msg = "Object not found",
        status = 404,
    },

    created_field = {
        msg = "Success",
        status = 201,
    },

    success_field = {
        msg = "Success",
        status = 200,
    },
}

-- luacheck: ignore self
function Response:create(req, json_msg, status)
    local resp = req:render({ json = json_msg })
    resp.status = status
    return resp
end

function Response:internal_err(req, err)
    local resp = self:create(req, {
        info = self.internal_err_field.msg,
        error = err,
    }, self.internal_err_field.status)
    return resp
end

function Response:incorrect_body_err(req)
    local resp = self:create(req, {
        info = self.incorrect_body_err_field.msg,
    }, self.incorrect_body_err_field.status)
    return resp
end

function Response:handle_err(req, err)
    if err.err == Response.key_exists_err_field.msg then
        return self:create(
            req,
            { info = err.err },
            Response.key_exists_err_field.status
        )
    elseif err.err == Response.not_found_err_field.msg then
        return self:create(
            req,
            { info = err.err },
            Response.not_found_err_field.status
        )
    else
        return Response:internal_err(req)
    end
end

return Response
