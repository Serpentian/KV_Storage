-- module for error throwing
local errors = require("errors")
local err_storage = errors.new_class("Storage error")
-- module for checking the arguments in the functions
local checks = require("checks")
-- module to exploit Tarantool's storage functionality
local box = require("box")
-- logging module
local log = require("log")

---------------------
---     Utils     ---
---------------------

local function to_table(format, tuple)
    local map = {}
    for idx, value in ipairs(format) do
        map[value.name] = tuple[idx]
    end
    return map
end

local function get_by_key(key)
    local obj = box.space.kv_storage:get(key)
    if obj then
        obj = to_table(box.space.kv_storage:format(), obj)
    end

    local err = (obj == nil) and err_storage:new("Object not found") or nil
    return obj, err
end

---------------------
--- Storage logic ---
---------------------

local function init_space()
    local kv_storage = box.schema.space.create(
        -- name of the space
        "kv_storage",
        {
            -- format for stored tuples
            format = {
                { name = "key", type = "string" },
                { name = "bucket_id", type = "unsigned" },
                { name = "value" },
            },
            -- creating the space only if it doesn't exist
            if_not_exists = true,
        }
    )

    -- creating an index by kv_storage's id
    kv_storage:create_index(
        -- index name
        "key",
        {
            parts = { "key" },
            if_not_exists = true,
        }
    )

    -- this one is for sharding
    kv_storage:create_index("bucket_id", {
        parts = { "bucket_id" },
        unique = false,
        if_not_exists = true,
    })
end

local function object_add(obj)
    checks("table")
    local exists = box.space.kv_storage:get(obj.key)
    if exists ~= nil then
        log.verbose("Tried to create object with diplicate key: " .. obj.key)
        return { ok = false, error = err_storage:new("Key already exists") }
    end

    box.space.kv_storage:insert({ obj.key, obj.bucket_id, obj.value })
    log.verbose("Added new object with key: " .. obj.key)
    return { ok = true, error = nil }
end

local function object_update(key, value)
    local obj, err = get_by_key(key)
    if err ~= nil then
        log.verbose("Tried to update object with nonexistent key: " .. key)
        return { obj = nil, error = err }
    end

    obj.value = value
    box.space.kv_storage:replace({ obj.key, obj.bucket_id, obj.value })
    log.verbose("Updated object with key: " .. key)

    obj.bucket_id = nil
    return { obj = obj, error = nil }
end

local function object_get(key)
    checks("string")
    local obj, err = get_by_key(key)
    if err ~= nil then
        log.verbose("Tried to get object with nonexistent key: " .. key)
        return { obj = nil, error = err }
    end

    obj.bucket_id = nil
    log.verbose("Giving away the object with key: " .. key)
    return { obj = obj, error = nil }
end

local function object_delete(key)
    checks("string")
    local _, err = get_by_key(key)
    if err ~= nil then
        log.verbose("Tried to delete object with nonexistent key: " .. key)
        return { ok = false, error = err }
    end

    box.space.kv_storage:delete(key)
    log.verbose("Deleted the object with key: " .. key)
    return { ok = true, error = nil }
end

----------------------------
--- Module configuration ---
----------------------------

local function init(opts)
    if opts.is_master then
        init_space()
        box.schema.func.create("object_add", { if_not_exists = true })
        box.schema.func.create("object_update", { if_not_exists = true })
        box.schema.func.create("object_get", { if_not_exists = true })
        box.schema.func.create("object_delete", { if_not_exists = true })
    end

    rawset(_G, "object_add", object_add)
    rawset(_G, "object_update", object_update)
    rawset(_G, "object_get", object_get)
    rawset(_G, "object_delete", object_delete)

    return true
end

return {
    role_name = "storage",
    init = init,
    utils = {
        object_add = object_add,
        object_update = object_update,
        object_get = object_get,
        object_delete = object_delete,
    },
    dependencies = {
        "cartridge.roles.vshard-storage",
    },
}
