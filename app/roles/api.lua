local cartridge = require("cartridge")
local errors = require("errors")
local box = require("box")
local log = require("log")

---------------------
---     Utils     ---
---------------------

local err_vshard = errors.new_class("Vshard routing error")
local err_httpd = errors.new_class("Httpd error")
local Response = require("app/utils/response")

local function get_sharding(id)
    local router = cartridge.service_get("vshard-router").get()
    local bucket_id = router:bucket_id_strcrc32(id)
    return router, bucket_id
end

---------------------
---   API logic   ---
---------------------

local function http_object_add(req)
    local status, obj = pcall(function()
        return req:json()
    end)

    if not status or not obj.key or not obj.value then
        log.verbose("Corrupted body for POST request")
        return Response:incorrect_body_err(req)
    end

    local router, bucket_id = get_sharding(obj.key)
    obj.bucket_id = bucket_id

    local resp, error = err_vshard:pcall(
        router.call,
        router,
        bucket_id,
        "write",
        "object_add",
        { obj }
    )

    if error then
        log.warn("Internal error encountered: " .. error.message)
        return Response:internal_err(req, error)
    end

    if resp.error then
        log.verbose("Storage error for request with key: " .. obj.key)
        log.verbose("Storage error: " .. resp.error.err)
        return Response:handle_err(req, resp.error)
    end

    local f = Response.created_field
    return Response:create(req, { info = f.msg }, f.status)
end

local function http_object_update(req)
    local key = tostring(req:stash("key"))
    local status, obj = pcall(function()
        return req:json()
    end)
    if not status or not obj.value then
        log.verbose("Corrupted body for object with key: " .. key)
        return Response:incorrect_body_err(req)
    end

    local value = obj.value
    local router, bucket_id = get_sharding(key)

    local resp, error = err_vshard:pcall(
        router.call,
        router,
        bucket_id,
        "read",
        "object_update",
        { key, value }
    )

    if error then
        log.warn("Internal error encountered: " .. error.message)
        return Response:internal_err(req, error)
    end

    if resp.error then
        log.verbose("Storage error for request with key: " .. key)
        log.verbose("Storage error: " .. resp.error.err)
        return Response:handle_err(req, resp.error)
    end

    local f = Response.success_field
    return Response:create(req, resp.obj, f.status)
end

local function http_object_get(req)
    local key = tostring(req:stash("key"))
    local router, bucket_id = get_sharding(key)
    local resp, error = err_vshard:pcall(
        router.call,
        router,
        bucket_id,
        "read",
        "object_get",
        { key }
    )

    if error then
        log.warn("Internal error encountered: " .. error.message)
        return Response:internal_err(req, error)
    end

    if resp.error then
        log.verbose("Storage error for request with key: " .. key)
        log.verbose("Storage error: " .. resp.error.err)
        return Response:handle_err(req, resp.error)
    end

    local f = Response.success_field
    return Response:create(req, resp.obj, f.status)
end

local function http_object_delete(req)
    local key = tostring(req:stash("key"))

    local router, bucket_id = get_sharding(key)
    local resp, error = err_vshard:pcall(
        router.call,
        router,
        bucket_id,
        "read",
        "object_delete",
        { key }
    )

    if error then
        log.warn("Internal error encountered: " .. error.message)
        return Response:internal_err(req, error)
    end

    if resp.error then
        log.verbose("Storage error for request with key: " .. key)
        log.verbose("Storage error: " .. resp.error.err)
        return Response:handle_err(req, resp.error)
    end

    local f = Response.success_field
    return Response:create(req, { info = f.msg }, f.status)
end

----------------------------
--- Module configuration ---
----------------------------

local function init(opts)
    if opts.is_master then
        box.schema.user.grant(
            "guest",
            "read,write,execute",
            "universe",
            nil,
            { if_not_exists = true }
        )
    end

    local httpd = cartridge.service_get("httpd")
    if not httpd then
        return nil, err_httpd:new("Httpd not found")
    end

    httpd:route(
        { path = "/kv", method = "POST", public = true },
        http_object_add
    )

    httpd:route(
        { path = "/kv/:key", method = "PUT", public = true },
        http_object_update
    )

    httpd:route(
        { path = "/kv/:key", method = "GET", public = true },
        http_object_get
    )

    httpd:route(
        { path = "/kv/:key", method = "DELETE", public = true },
        http_object_delete
    )

    return true
end

return {
    role_name = "api",
    init = init,
    dependencies = {
        "cartridge.roles.vshard-router",
    },
}
