#!/usr/bin/env tarantool

require("strict").on()

if package.setsearchroot ~= nil then
    package.setsearchroot()
else
    local fio = require("fio")
    local app_dir = fio.abspath(fio.dirname(arg[0]))
    package.path = app_dir .. "/?.lua;" .. package.path
    package.path = app_dir .. "/?/init.lua;" .. package.path
    package.path = app_dir .. "/.rocks/share/tarantool/?.lua;" .. package.path
    package.path = app_dir
        .. "/.rocks/share/tarantool/?/init.lua;"
        .. package.path
    package.cpath = app_dir .. "/?.so;" .. package.cpath
    package.cpath = app_dir .. "/?.dylib;" .. package.cpath
    package.cpath = app_dir .. "/.rocks/lib/tarantool/?.so;" .. package.cpath
    package.cpath = app_dir .. "/.rocks/lib/tarantool/?.dylib;" .. package.cpath
end

require("cartridge.stateboard").cfg()
