package = 'kv_storage'
version = 'scm-1'
source  = {
    url = '/dev/null',
}
dependencies = {
    'tarantool',
    'lua >= 5.1',
    'checks == 3.1.0-1',
    'metrics == 0.12.0-1',
    'cartridge == 2.7.3-1',
    'cartridge-cli-extensions == 1.1.1-1',
}
build = {
    type = 'none';
}
